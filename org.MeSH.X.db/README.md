# org.MeSH.Mmu.db
org.MeSH.Mmu.db is a R/Bioconductor package for providing a mapping table
of Entrez Gene ID to MeSH ID.

## Install
sudo R CMD INSTALL org.MeSH.Mmu.db_0.99.0.tar.gz

## Usage
```
library("org.MeSH.Mmu.db")

ls("package:org.MeSH.Mmu.db")

example(geneid2meshid)

keytypes(GeneID2MeSHID)
columns(GeneID2MeSHID)

# Query    
my.keytype <- c("gene_id")
gene_ids <- keys(GeneID2MeSHID, keytype = my.keytype)
	
my.keys    <- c(1, 2, 3)
my.columns <- c("gene_id", "mesh_id")
my.mesh    <- select(
  GeneID2MeSHID,
  keys = my.keys,
  columns = my.columns,
  keytype = my.keytype
)
head(my.mesh)

citation("org.MeSH.Mmu.db")
```
